import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import * as fromServices from './services';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HeroPowerService} from './services/hero-power.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [...fromServices.services, HeroPowerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
