import { HeroService } from './hero.service';
import { PowerService } from './power.service';

export const services: any[] = [HeroService, PowerService];

export * from './hero.service';
export * from './power.service';
